#!/bin/bash
set -e

usage() {
  echo "$0 <namespace-name>"
  exit 1
}

if [ ! $# -eq 1 ]; then
  usage
fi

ns=$1

kubectl get secret -n "$ns" --no-headers | grep -vE '^default-token-' |awk '{ print $1}'|
while read sn; do
  echo "Exporting secret '$sn'"
  kubectl get secret "$sn" -n "$ns" -o yaml --export > "${sn}.yaml"
done
